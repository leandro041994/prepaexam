# Le déroulé de l’examen

> **Ca y est, tu as préparé tous tes dossiers, révisé ta présentation, tu es fin prêt(e) pour le jour J ! Le jour de l’examen,l’épreuve va durer 1h30. La salle sera équipée d’un vidéo-projecteur, d’un ordinateur, d’un accès internet et d’un tableau blanc. L’épreuve se décompose en trois parties.**

## 1. Présentation d’un projet réalisé pendant la session (35 min)

> **Il s’agit de la présentation de ton projet en entreprise :**

* Commence par faire un résumé en à l’oral de ton projet, sans lire l’abstract de ton dossier projet mais en ayant bien préparé et répété en amont cette partie.

* Présente ensuite ton projet à l'aide du support que tu as préparé. Pense à bien faire une présentation technique du code développé pour rendre accessible et/ou stocker la donnée, notamment dans le cas d’utilisation de framework de type « nodb » ou de
framework intégrant une gestion native des liaisons avec la base de donnée. Par exemple, savoir expliquer/montrer le code gérant le schéma de la base, les classes objets représentant les tables et les méthodes représentant les requêtes insertion/modification/suppression. Idem si l’orientation choisie pour le développement est procédurale

* Inclus au choix dans ta présentation une illustration ou une démonstration de l'interface de l'application (cette démonstration ne doit pas dépasser 10 min).

### 2. Entretien technique (40 min)

> **L’entretien technique permet au jury de te questionner sur la base du dossier projet et de la présentation orale que tu viens juste de terminer afin de s'assurer de la maîtrise des compétences couvertes par le projet.**

#### 3. Entretien final (15 min)

> **Les 15 dernières minutes sont consacrées à un temps d'échange entre le jury et toi sur ton dossier professionnel (DP) ainsi que sur ta vision et la culture professionnelle que tu as sur le métier de développeur.**